package co.axacolpatria.portalaliados.PortalAliados;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.imageio.ImageIO;


import java.awt.Desktop;
import java.util.Date;
import javax.swing.JOptionPane;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Driver {
	public String pathCapturas;
	public String pathCarpetas;
	public int numFilasArchivo;
	public String pathArchivoXlsx;

	public WebDriver driver = new ChromeDriver();
	
	public Driver() {
		
		String url = "http://gestion.qa.axacolpatria.co:8080/home";
		
		driver.get(url);
		driver.manage().window().maximize();
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability("marionette", true);
		cap.setBrowserName("chrome");
		System.out.println("Entrando a " + url);

	}
	public void takeScreenShotTest(String imageName) {
		try {
			   Robot robot = new Robot();
		       BufferedImage screenShot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		       //obtener fecha
		       LocalDate date = java.time.LocalDate.now();
		       String fecha = date.toString();
		       LocalDateTime realTime= LocalDateTime.now();
		       //concatenar para obtener nombres únicos para las imágenes
		       String nombre = imageName+"H"+realTime.getHour()+"M"+realTime.getMinute()+"S"+realTime.getSecond()+"-"+fecha+".png";
		       ImageIO.write(screenShot, "PNG", new File(this.pathCarpetas, nombre));
		} catch(Exception e) {
			e.printStackTrace();
		}
 }
	public void terminar() {
		driver.close();
	}
	public void leerArchivo() {
		 try {
			 String sCarpAct = System.getProperty("user.home");
			 String otherFolder = sCarpAct + File.separator + "Downloads";
			 File carpeta = new File(otherFolder);
			 File[] archivos = carpeta.listFiles();
			 if (archivos == null || archivos.length == 0) {
			  System.out.println("No hay elementos dentro de la carpeta actual");
			  return;
			 }
			 else {
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");// HH:mm:ss
			  Date fechaInicial = sdf.parse("0000-00-00 00:00:00");
			  String path = null;
			  for (int i=0; i< archivos.length; i++) {
			     File archivo = archivos[i];
			     //leer fichero
			  
		        Date fechaArchivo = sdf.parse(sdf.format(archivo.lastModified()));


		        if (fechaArchivo.compareTo(fechaInicial) > 0) {
		        	fechaInicial = fechaArchivo;
		          System.out.println("fecha mayor : " + sdf.format(fechaArchivo));
		          path = archivo.getPath();
		        } 

			     /*imprimir información del archivo
			     System.out.println(String.format("%s (%s) - %d - %s", 
			                        archivo.getName(), 
			                        archivo.isDirectory() ? "Carpeta" : "Archivo",
			                        archivo.length(),
			                        sdf.format(archivo.lastModified())
			                       ));*/
			   }
			  Desktop ficheroAEjecutar=Desktop.getDesktop();
			     try {
			         ficheroAEjecutar.open(new File(path));
			    } catch (IOException e) {
			        JOptionPane.showMessageDialog(null, 
			                                       e.getMessage(), 
			                                       "Error", 
			                                       JOptionPane.ERROR_MESSAGE);
			    }
			  
			 }
		 } catch(Exception e) {e.printStackTrace();}
	}
	public void crearCarpeta() {
		File directorio = new File(this.pathCarpetas); 
		if(!directorio.exists()) {
			directorio.mkdir();
		}else {
		   if(calcularTamañoCarpeta(directorio)>314572800) {
		       LocalDate date = java.time.LocalDate.now();
		       String fecha = date.toString();
			   this.pathCarpetas+=fecha;
			   directorio = new File(this.pathCarpetas); 
			   directorio.mkdir();
			   System.out.println("¡La carpeta supera el tamaño permitido!");
		    }
		}
	}
	public XSSFSheet leerArchivoXlsx() throws IOException {
		//lectura de archivo .xlsx
		File file = new File(this.pathArchivoXlsx);

		FileInputStream Fis = new FileInputStream(file);

		XSSFWorkbook wb = new XSSFWorkbook(Fis);

		XSSFSheet sheet1 = wb.getSheetAt(0);

	//	String data0 = sheet1.getRow(0).getCell(0).getStringCellValue();
	//  String data1 = String.valueOf(sheet1.getRow(0).getCell(1).getRawValue());

        this.numFilasArchivo = sheet1.getPhysicalNumberOfRows();
       //int noOfColumns = sheet1.getRow(0).getPhysicalNumberOfCells();
        wb.close();
        return sheet1;
	}
	public long calcularTamañoCarpeta(File directory) {
	    long length = 0;
	    for (File file : directory.listFiles()) {
	        if (file.isFile())
	            length += file.length();
	        else
	            length += calcularTamañoCarpeta(file);
	    }
	    return length;
	}
}
