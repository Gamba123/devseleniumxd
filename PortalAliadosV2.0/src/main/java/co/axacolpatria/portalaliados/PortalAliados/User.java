package co.axacolpatria.portalaliados.PortalAliados;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;

public class User {

	public LoginPageObject loginPageObject;
	
	public User() throws InterruptedException {
		this.loginPageObject = new LoginPageObject();
	}
	public void user01() throws InterruptedException, AWTException, IOException {
		//path archivo excel
		String pathArchivo = System.getProperty("user.dir")+"\\Archivos\\portalAliados.xlsx";
		this.loginPageObject.pathArchivoXlsx = pathArchivo;
		//inicializar ruta de im�genes
		this.loginPageObject.pathCapturas = this.loginPageObject.leerArchivoXlsx().getRow(2).getCell(0).getStringCellValue();
		//ingresar datos para inicio de sesi�n
		this.loginPageObject.WriteUserName(this.loginPageObject.leerArchivoXlsx().getRow(3).getCell(0).getStringCellValue());
		this.loginPageObject.WritePassword(this.loginPageObject.leerArchivoXlsx().getRow(3).getCell(1).getStringCellValue());
		this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\inicioSesion";
		this.loginPageObject.crearCarpeta();
		this.loginPageObject.takeScreenShotTest("login");
		this.loginPageObject.ClickatTheBotton();
		this.loginPageObject.IconoReportes();
		Thread.sleep(1000);
		this.ejecucionProcesosPortalGestion();		
		this.loginPageObject.terminar();
	}
	
	public void ejecucionProcesosPortalGestion() throws NumberFormatException, IOException, InterruptedException {
		//recorrer columnas de cabecera del archivo, para determinar opciones del usuario
				for(int posicionCol = 0; posicionCol < (this.loginPageObject.leerArchivoXlsx().getRow(0).getPhysicalNumberOfCells()); posicionCol++) {
					if(posicionCol >= 2) {
						int num = 0;
						//Determinar opci�n del usuario
						switch(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(posicionCol).getStringCellValue()) {
							case "reportesRUNT":
								//repetir ejecucion segun se dese� - informacion de repeticiones se obtiene del excel
								num = Integer.parseInt(this.loginPageObject.leerArchivoXlsx().getRow(1).getCell(posicionCol).getRawValue());
								for(int repetirEjecucion = 0; repetirEjecucion<num; repetirEjecucion++) {
									this.loginPageObject.ReportesRuntButton();
									//asignar valor fecha para los inputs
									this.loginPageObject.WritDate(this.loginPageObject.leerArchivoXlsx().getRow(4).getCell(repetirEjecucion).getStringCellValue());
									this.loginPageObject.DateRUNT();
									this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\reporteRUNT";
									this.loginPageObject.crearCarpeta();
									/*buscar reporteRUNT - recorrer filas para encontrar el dato que relaciona con reportes RUNT
									  Al encontrarlo sumar uno para tomar la columna que le sigue*/
									for(int posicionFila = 0; posicionFila < this.loginPageObject.leerArchivoXlsx().getPhysicalNumberOfRows(); posicionFila++) {
										for(int posicionCol2 = 0; posicionCol2 < this.loginPageObject.leerArchivoXlsx().getRow(posicionFila).getPhysicalNumberOfCells(); posicionCol2++) {
											DataFormatter formatter = new DataFormatter();
											String value = formatter.formatCellValue(this.loginPageObject.leerArchivoXlsx().getRow(posicionFila).getCell(posicionCol2));
											if("reportesRUNTData".equals(value)) {
												//leer opci�n para RUNT consultar o generar	seg�n se haya definido en el archivo						
												String opcionRUNT = this.loginPageObject.leerArchivoXlsx().getRow(posicionFila).getCell(posicionCol2+1).getStringCellValue();
												if(opcionRUNT.equals("consultar")) {
													this.loginPageObject.ButtonConsultarReporteRuntClick();
												}else {
													this.loginPageObject.ButtonGenerarReporteRuntClick();
												}
												Thread.sleep(3000);										
											}
										
										}
									}
								
								}
								break;
							case "reportesEmision":
								//repetir ejecucion segun se dese�
								num = Integer.parseInt(this.loginPageObject.leerArchivoXlsx().getRow(1).getCell(posicionCol).getRawValue());
								for(int repetirEjecucion = 0; repetirEjecucion<num; repetirEjecucion++) {
									//asignar valor fecha para los inputs
									this.loginPageObject.WritDate(this.loginPageObject.leerArchivoXlsx().getRow(4).getCell(repetirEjecucion).getStringCellValue());
									this.loginPageObject.IconoReportes();
									this.loginPageObject.ReporteEmisionClick();						
									this.loginPageObject.InsertarDateReporteEmision();
									this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\reporteEmision";
									this.loginPageObject.crearCarpeta();
									this.loginPageObject.ButtonGenerarReporteEmisionClick();
									Thread.sleep(3000);
								}
								break;
							case "reporteCotizacion":
								//repetir ejecucion segun se dese�
								num = Integer.parseInt(this.loginPageObject.leerArchivoXlsx().getRow(1).getCell(posicionCol).getRawValue());
								for(int repetirEjecucion = 0; repetirEjecucion<num; repetirEjecucion++) {
									//asignar valor fecha para los inputs
									this.loginPageObject.WritDate(this.loginPageObject.leerArchivoXlsx().getRow(4).getCell(repetirEjecucion).getStringCellValue());
									this.loginPageObject.IconoReportes();
									this.loginPageObject.ReporteCotizacionClick();
									Thread.sleep(3000);
									this.loginPageObject.InsertarDateReporteCotizacion();
									this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\reporteCotizacion";
									this.loginPageObject.crearCarpeta();
									this.loginPageObject.GenerarReporteCotizacionClick();
									Thread.sleep(3000);
								}
								break;
							default: 
								System.out.println("�ERROR! La opci�n seleccionada no se encuentra registrada en el programa.");
								
						}
				
					}			
					
				}
	}
}
