package co.axacolpatria.portalaliados.PortalAliados;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFileChooser;

public class seleccionarArchivo extends JFrame {

	private JPanel contentPane;
	public String nombreArchivo;
	public String nombreCarpeta;

	/**
	 * Launch the application.
	 */
	public void correrPrueba() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					seleccionarArchivo frame = new seleccionarArchivo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @return 
	 * @throws InterruptedException 
	 */
	public seleccionarArchivo() throws InterruptedException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JFileChooser fileChooser = new JFileChooser();
		//Abrimos la ventana, guardamos la opcion seleccionada por el usuario
				int seleccion=fileChooser.showOpenDialog(contentPane);
				 
				//Si el usuario, pincha en aceptar
				if(seleccion==JFileChooser.APPROVE_OPTION){
				 
				    //Seleccionamos el fichero
				    File fichero=fileChooser.getSelectedFile();
				  
				 
				    //Ecribe la ruta del fichero seleccionado en el campo de texto
				    this.nombreArchivo = fichero.getAbsolutePath();
				    this.nombreCarpeta = fichero.getParentFile().toString();
				    System.out.println(fichero.getAbsoluteFile());
				    System.out.println(fichero.getParentFile());		    
				 
				}else {
					JOptionPane.showMessageDialog(null, "Por favor, seleccionar un archivo.");			
					System.exit(ERROR);
				}
		contentPane.add(fileChooser, BorderLayout.NORTH);
	}

}
