package co.axacolpatria.portalaliados.PortalAliados;

import java.awt.AWTException;
import java.io.IOException;
import org.junit.Assert;
public class User {

	public LoginPageObject loginPageObject;
	
	public User() throws InterruptedException {
		this.loginPageObject = new LoginPageObject();
	}
	public void user01(String nameFile, String nameFolder) throws InterruptedException, AWTException, IOException {
		//path archivo excel
		try {
			Assert.assertTrue("Good test!", true);
			this.loginPageObject.pathArchivoXlsx = nameFile;
			for(int repetirFilas= 3; repetirFilas<(this.loginPageObject.leerArchivoXlsx().getPhysicalNumberOfRows()-1); repetirFilas++) {
				
				this.loginPageObject.abrirNavegador();
				//inicializar ruta de im�genes
				this.loginPageObject.pathCapturas = this.loginPageObject.leerArchivoXlsx().getRow(2).getCell(0).getStringCellValue();
				//ingresar datos para inicio de sesi�n
				this.loginPageObject.WriteUserName(this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(0).getStringCellValue());
				this.loginPageObject.WritePassword(this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(1).getStringCellValue());
				this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\inicioSesion";
				this.loginPageObject.crearCarpeta();
				this.loginPageObject.takeScreenShotTest("login");
				this.loginPageObject.ClickatTheBotton();
			
				this.ejecucionProcesosPortalGestion(nameFolder);		
				this.loginPageObject.cerrarSesionClick();
			
			}
			this.loginPageObject.terminar();
			 System.exit(0);
			 
		}catch(Exception e) {
			e.printStackTrace();
		}
	   
	}
	
	public void ejecucionProcesosPortalGestion(String folder) throws NumberFormatException, IOException, InterruptedException {
		try {
		//recorrer columnas de cabecera del archivo, para determinar opciones del usuario
				for(int posicionCol = 0; posicionCol < (this.loginPageObject.leerArchivoXlsx().getRow(0).getPhysicalNumberOfCells()); posicionCol++) {
				
						int num = 0;
						
						num = Integer.parseInt(this.loginPageObject.leerArchivoXlsx().getRow(1).getCell(posicionCol).getRawValue());			
						//Determinar opci�n del usuario
						switch(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(posicionCol).getStringCellValue()) {
							case "reportesRUNT":	
								//repetir ejecucion segun se dese� - informacion de repeticiones se obtiene del excel
								for(int repetirEjecucion = 0; repetirEjecucion<num; repetirEjecucion++) {
									
									this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\reporteRUNT";
									this.loginPageObject.crearCarpeta();
									//asignar valor fecha para los inputs
									this.loginPageObject.pathArchivoXlsx = folder+"\\reporteRUNT.xlsx";	
									//recorrer fila de archivo seleccionado
										for(int repetirFilas= 1; repetirFilas<(this.loginPageObject.leerArchivoXlsx().getPhysicalNumberOfRows()); repetirFilas++) {
											this.loginPageObject.IconoReportes();
											Thread.sleep(1000);
											this.loginPageObject.ReportesRuntButton();
											//recorrer columna de archivo
											for(int recorrerFilaRUNT= 0; recorrerFilaRUNT<(this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getPhysicalNumberOfCells()); recorrerFilaRUNT++) {
												org.apache.poi.ss.usermodel.CellType type = this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaRUNT).getCellTypeEnum();
												//determinar opcion
													if(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaRUNT).getStringCellValue().equals("opcion")) 
												   {
														//leer opci�n para RUNT consultar o generar	seg�n se haya definido en el archivo
														String opcionRUNT = this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(4).getStringCellValue();						
														if(opcionRUNT.equals("consultar")) {
															this.loginPageObject.ButtonConsultarReporteRuntClick();
														}else {
															this.loginPageObject.ButtonGenerarReporteRuntClick(0);
														}
														Thread.sleep(3000);							
												   }else if (type == org.apache.poi.ss.usermodel.CellType.STRING) {
													  this.loginPageObject.insertarValorElementoRUNT(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaRUNT).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaRUNT).getStringCellValue()); 
								                    } else if (type == org.apache.poi.ss.usermodel.CellType.NUMERIC) {
								                    	 this.loginPageObject.insertarValorElementoRUNT(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaRUNT).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaRUNT).getRawValue());
								                    }	
											}	
										}
							
									}
								break;
							case "reportesEmision":
								//repetir ejecucion segun se dese� - informacion de repeticiones se obtiene del excel
								for(int repetirEjecucion = 0; repetirEjecucion<num; repetirEjecucion++) {
									//asignar valor fecha para los inputs
									this.loginPageObject.pathArchivoXlsx = folder+"\\reporteEmision.xlsx";
									for(int repetirFilas= 1; repetirFilas<(this.loginPageObject.leerArchivoXlsx().getPhysicalNumberOfRows()); repetirFilas++) {
										this.loginPageObject.IconoReportes();
										this.loginPageObject.ReporteEmisionClick();			
										
										for(int recorrerFilaEmision= 0; recorrerFilaEmision<(this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getPhysicalNumberOfCells()); recorrerFilaEmision++) {
											org.apache.poi.ss.usermodel.CellType type = this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaEmision).getCellTypeEnum();
											//asignar lista
											if(recorrerFilaEmision>=7 && type != null) {								
												this.loginPageObject.seleccionarListaEmision(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaEmision).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaEmision).getRawValue());
											}  
											else if (type == org.apache.poi.ss.usermodel.CellType.STRING) {
												  this.loginPageObject.insertarValorElementoEmision(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaEmision).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaEmision).getStringCellValue()); 
							                   } else if (type == org.apache.poi.ss.usermodel.CellType.NUMERIC) {
							                    	 this.loginPageObject.insertarValorElementoEmision(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaEmision).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaEmision).getRawValue());
							                   }	
										}										
									
										this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\reporteEmision";
										this.loginPageObject.crearCarpeta();
										this.loginPageObject.ButtonGenerarReporteEmisionClick();
										Thread.sleep(3000);
									}
								}
								break;
							case "reporteCotizacion":
								//repetir ejecucion segun se dese� - informacion de repeticiones se obtiene del excel
								for(int repetirEjecucion = 0; repetirEjecucion<num; repetirEjecucion++) {
									//asignar valor fecha para los inputs
									this.loginPageObject.pathArchivoXlsx = folder+"\\reporteCotizacion.xlsx";
									//recorrer fila
									for(int repetirFilas= 1; repetirFilas<(this.loginPageObject.leerArchivoXlsx().getPhysicalNumberOfRows()); repetirFilas++) {
										this.loginPageObject.IconoReportes();
										this.loginPageObject.ReporteCotizacionClick();
										//recorrer columnas
										for(int recorrerFilaCotizacion= 0; recorrerFilaCotizacion<(this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getPhysicalNumberOfCells()); recorrerFilaCotizacion++) {
											org.apache.poi.ss.usermodel.CellType type = this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaCotizacion).getCellTypeEnum();
											 if (type == org.apache.poi.ss.usermodel.CellType.STRING) {
												  this.loginPageObject.insertarValorElementoCotizacion(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaCotizacion).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaCotizacion).getStringCellValue()); 
							                   } else if (type == org.apache.poi.ss.usermodel.CellType.NUMERIC) {
							                	   this.loginPageObject.insertarValorElementoCotizacion(this.loginPageObject.leerArchivoXlsx().getRow(0).getCell(recorrerFilaCotizacion).getStringCellValue(), this.loginPageObject.leerArchivoXlsx().getRow(repetirFilas).getCell(recorrerFilaCotizacion).getRawValue());
							                   }	
										}										
									
										this.loginPageObject.pathCarpetas=this.loginPageObject.pathCapturas+"\\reporteCotizacion";
										this.loginPageObject.crearCarpeta();
										this.loginPageObject.GenerarReporteCotizacionClick();
										Thread.sleep(3000);				
									}
								}
								break;								
							default: 
								System.out.println("�ERROR! La opci�n seleccionada no se encuentra registrada en el programa.");								
								break;
						}
						this.loginPageObject.pathArchivoXlsx = folder+"\\portalAliados.xlsx";
					
				}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
