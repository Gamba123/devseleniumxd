package co.axacolpatria.portalaliados.PortalAliados;

import java.awt.AWTException;
import java.io.IOException;
import org.junit.Assert;
//import org.openqa.selenium.remote.DesiredCapabilities;

public class APP {

	public static void main(String[] args) throws InterruptedException, AWTException, IOException {
		//seleccionar archivo por medio de interfaz
		seleccionarArchivo primerArchivo = new seleccionarArchivo();
		primerArchivo.correrPrueba();
		//DesiredCapabilities caps = new DesiredCapabilities();
		String pathDriver = System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe";
		//asignar driver
		System.setProperty("webdriver.chrome.driver", pathDriver);
		User PrimerUsuario = new User();
	    PrimerUsuario.user01(primerArchivo.nombreArchivo, primerArchivo.nombreCarpeta);
	    System.exit(0);
	}

}
