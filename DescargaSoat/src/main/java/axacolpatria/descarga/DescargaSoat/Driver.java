package axacolpatria.descarga.DescargaSoat;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.google.common.io.Files;

public class Driver {

	public WebDriver driver;
	
	public Driver() {

		String url = "http://clientes.qa.axacolpatria.co:8081/web/my-axa/soat/descargar";

		ChromeOptions options = new ChromeOptions();
		options.setHeadless(false);
		driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		
		options.setCapability("marionette", true);
		System.out.println("Entrando a " + url);
	}

	public static void takeScreenShotTest(WebDriver driver, String imageName) {
		// Directorio en donde quedaran ubicadas las imagenes guardadas//
		File directory = new File("C:\\Users\\cagambasicas\\Desktop\\PortalAliados");

		try {

			if (directory.isDirectory()) {
				File imagen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				// Mueve el archivo a la carga especificada con el respectivo nombre//

				Files.copy(imagen, new File(directory.getAbsolutePath() + "\\" + imageName + ".png"));

			} else {
				// Se lanza la excepcion cuando no encuentre el directorio//
				throw new IOException("ERROR: la ruta especificada no es un directorio");

			}
		} catch (IOException e) {

			// Impresion de excepcciones//

			e.printStackTrace();

		}
	}
}
