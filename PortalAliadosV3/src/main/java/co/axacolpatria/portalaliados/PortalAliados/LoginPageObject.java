package co.axacolpatria.portalaliados.PortalAliados;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

public class LoginPageObject extends Driver {
	private String fecha;

	public LoginPageObject() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;

	}
    // -------------------------------------------WebElements-------------------------------------------------//

	public WebElement UserName() {
		return getDriver().findElement((By.id("_Authentication_username")));

	}

	public WebElement Password() {
		return getDriver().findElement((By.id("_Authentication_password")));

	}

	public WebElement BottonAcceder() {
		return getDriver().findElement((By.tagName("button")));		
	}
	public WebElement elementoRUNT(String nombreElemento) {
		return getDriver().findElement((By.xpath("//*[@id=\"_SoatRuntReportPortlet_"+nombreElemento+"\"]")));		
	}
	public WebElement elementoEmision(String nombreElemento) {
		//*[@id="_SoatEmissionReportPortlet_emissionDate"]
		//*[@id="_SoatEmissionReportPortlet_startDateValidity"]
		//*[@id="_SoatEmissionReportPortlet_endDateValidity"]
		//*[@id="_SoatEmissionReportPortlet_agreementKey"]
		//*[@id="_SoatEmissionReportPortlet_pointSellingCode"]
		//*[@id="_SoatEmissionReportPortlet_branchOfficeName"]
		//*[@id="_SoatEmissionReportPortlet_yearModel"]
		//*[@id="_SoatEmissionReportPortlet_vehicleTypeCode"]
		//*[@id="_SoatEmissionReportPortlet_registrationAreaCode"]
		//*[@id="_SoatEmissionReportPortlet_distributionAreaCode"]
		return getDriver().findElement((By.xpath("//*[@id=\"_SoatEmissionReportPortlet_"+nombreElemento+"\"]")));		
	}
	public WebElement elementoCotizacion(String nombreElemento) {
		//*[@id="_SoatPriceQuotesReportPortlet_priceQuoteDate"]
		return getDriver().findElement((By.xpath("//*[@id=\"_SoatPriceQuotesReportPortlet_"+nombreElemento+"\"]")));		
	}
	public WebElement cerrarSesion() {
		return getDriver().findElement((By.xpath("//*[@id=\"banner\"]/div[1]/div/div/div/a")));		
	}

	public WebElement IconoReporte() throws InterruptedException {
		Thread.sleep(1000);
		return getDriver().findElement((By.id("jbfs____")));

	}

	public void IconoReportes() throws InterruptedException {
		Thread.sleep(3000);
		final WebElement IconoReportes = driver.findElement(By.id("layout_73562"));
		IconoReportes.click(); 
		Thread.sleep(1000);

	}

	public WebElement ReportesRunt() throws InterruptedException {
		Thread.sleep(1000);
		return getDriver().findElement(By.id("layout_73564"));
		

	}

	public WebElement ReporteEmision() {

		return getDriver().findElement(By.id("layout_73563"));

	}

	public WebElement ReporteCotizacion() throws InterruptedException {
		Thread.sleep(3000);
		return getDriver().findElement(By.id("layout_75346"));

	}

	public WebElement Date() {

		return getDriver().findElement(By.name("_SoatRuntReportPortlet_reportDate"));

	}

	public WebElement DateReporteEmision() {

		return getDriver().findElement(By.name("_SoatEmissionReportPortlet_emissionDate"));

	}
	public WebElement DateReporteCotizacion() {

		return getDriver().findElement(By.name("_SoatPriceQuotesReportPortlet_priceQuoteDate"));

	}

	public WebElement ButtonGenerarReporteRunt() {

		return getDriver().findElement(By.className("btn-test"));

	}	
	public WebElement ButtonGenerarReporteCotizacion() {

		return getDriver().findElement(By.className("btn-primary"));

	}

	public WebElement ButtonConsultarReporteRunt() {

		return getDriver().findElement(By.className("btn-query"));

	}	
	public WebElement ButtonDescargaReporteRunt() {

		return getDriver().findElement(By.className("btn-download"));

	}	

	public WebElement ButtonGenerarReporteEmision() {

		return getDriver().findElement(By.className("btn-primary"));

	}

	public WebElement ButtonConsultarReporte() {

		return getDriver().findElement(By.className("btn-report"));

	}

	public WebElement ButtonDownload() {

		return getDriver().findElement(By.className("btn-download"));

	}
	//-----------------------------------------------Services-------------------------------------------------//

	public void WriteUserName(String Name) {
		UserName().sendKeys(Name);

	}

	public void WritePassword(String Password) {
		Password().sendKeys(Password);

	}

	public void ClickatTheBotton() throws InterruptedException {
		BottonAcceder().click();
		Thread.sleep(1000);
	}

	public void IconoReportesClick() throws InterruptedException {
		WebDriverWait wait1 = new WebDriverWait(driver, 1000);
		wait1.until(ExpectedConditions.visibilityOf(IconoReporte()));
		Thread.sleep(1000);
		IconoReporte().click();
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOf(ReporteEmision()));
		Thread.sleep(1000);
	}

	public void ReportesRuntButton() throws InterruptedException {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOf(ReportesRunt()));
		ReportesRunt().click();

	}

	public void WritDate(String DateWrite) throws InterruptedException {
		try {
			this.fecha = DateWrite;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void DateRUNT() throws InterruptedException {
			Thread.sleep(1000);
			Date().sendKeys(this.fecha);
			Thread.sleep(1000);
	}
	public void insertarValorElementoRUNT(String elemento, String valor) throws InterruptedException {
		Thread.sleep(1000);
		this.elementoRUNT(elemento).sendKeys(valor);
		Thread.sleep(1000);
	}
	public void insertarValorElementoEmision(String elemento, String valor) throws InterruptedException {
		Thread.sleep(1000);
		this.elementoEmision(elemento).sendKeys(valor);
		Thread.sleep(1000);
	}
	public void insertarValorElementoCotizacion(String elemento, String valor) throws InterruptedException {
		Thread.sleep(1000);
		this.elementoCotizacion(elemento).sendKeys(valor);
		Thread.sleep(1000);
	}

	public void InsertarDateReporteEmision() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 5000);
		wait.until(ExpectedConditions.visibilityOf(DateReporteEmision()));
		this.DateReporteEmision().sendKeys(this.fecha);
		Thread.sleep(1000);

	}
	public void InsertarDateReporteCotizacion() throws InterruptedException {
		Thread.sleep(2000);
		this.DateReporteCotizacion().sendKeys(this.fecha);
		Thread.sleep(1000);

	}

	public void ButtonGenerarReporteRuntClick(int num) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 3000);
			wait.until(ExpectedConditions.visibilityOf(ButtonGenerarReporteRunt()));
			ButtonGenerarReporteRunt().click();
			Thread.sleep(2000);
			
			//final WebElement IconoError = driver.findElement(By.className("alert-danger"));
			Thread.sleep(2000);
			Boolean isPresent = driver.findElements(By.className("alert-danger")).size() > 0 ;
			System.out.println(isPresent);
			
			if (isPresent) {
				System.out.println("Es posible que ya se encuentre generada, por favor dar click a CONSULTAR.");
				isPresent = false;
			}else {
				isPresent = true;
				System.out.println("Generada.");
			}
			if(num == 1 && isPresent == false) {
				System.out.println("�Fechas invalidas para consultar!");
				return;
			}
			this.takeScreenShotTest("generarRUNT"+isPresent);
			this.ButtonConsultarReporteRuntClick();
		
	}
	
	public void ButtonConsultarReporteRuntClick() throws InterruptedException {
	
			WebDriverWait wait = new WebDriverWait(driver, 3000);
			wait.until(ExpectedConditions.visibilityOf(ButtonConsultarReporteRunt()));
			ButtonConsultarReporteRunt().click();			
			Thread.sleep(5000);
			Boolean isPresent = driver.findElements(By.className("alert-danger")).size() > 0 ;
			if(isPresent == false) {
				isPresent = true;
				this.takeScreenShotTest("consultarRUNT"+isPresent);
				System.out.println("�Exitoso proceso RUNT!");
				this.ButtonDescargarReporteRuntClick();
				
			}else {
				isPresent = false;
				System.out.println("Es posible que no se encuentre generada, por favor generarla.");
				this.takeScreenShotTest("consultarRUNT"+isPresent);
				this.ButtonGenerarReporteRuntClick(1);
			}
			
		//	this.ButtonDescargarReporteRuntClick();
		
		
	}
	public void ButtonDescargarReporteRuntClick() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		wait.until(ExpectedConditions.visibilityOf(ButtonDescargaReporteRunt()));
		ButtonDescargaReporteRunt().click();
		Thread.sleep(3000);
		//this.leerArchivo();
		this.takeScreenShotTest("decargaRUNT");
	}

	public void ButtonGenerarReporteEmisionClick() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		wait.until(ExpectedConditions.visibilityOf(ButtonGenerarReporteEmision()));
		Thread.sleep(3000);
		ButtonGenerarReporteEmision().click();
		Thread.sleep(2000);
		Boolean isPresent = driver.findElements(By.className("alert-danger")).size() > 0 ;
		if (isPresent) {
			System.out.println("�Error en generar el reporte de emisi�n!");
			isPresent = false;
		} else {
			System.out.println("�Exitoso proceso emisi�n!");
			isPresent = true;
		}
		this.takeScreenShotTest("generarReporteEmision"+isPresent);
		
	}
	public void ReporteEmisionClick() throws InterruptedException {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOf(ReporteEmision()));
		Thread.sleep(1000);
		ReporteEmision().click();

	}
	
	public void ReporteCotizacionClick() throws InterruptedException {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		wait.until(ExpectedConditions.visibilityOf(ReporteCotizacion()));
		Thread.sleep(1000);
		ReporteCotizacion().click();

	}
	public void cerrarSesionClick() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOf(cerrarSesion()));
		Thread.sleep(1000);
		cerrarSesion().click();

	}
	public void seleccionarListaEmision(String nameList, String nameElementList) throws InterruptedException {
		Select emision = new Select (this.elementoEmision(nameList));
		emision.selectByValue(nameElementList);
	}
	public void GenerarReporteCotizacionClick() throws InterruptedException {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOf(ButtonGenerarReporteCotizacion()));
		Thread.sleep(1000);
		ButtonGenerarReporteCotizacion().click();
		Thread.sleep(2000);
		Boolean isPresent = driver.findElements(By.className("alert-danger")).size() > 0 ;
		
		if (isPresent) {
			System.out.println("�Error en generar el reporte de cotizaci�n!");
			isPresent = false;
		}else {
			System.out.println("�Exitoso proceso cotizaci�n!");
			isPresent = true;
		}
		this.takeScreenShotTest("generarReporteCotizacion"+isPresent);
		
	}

}
