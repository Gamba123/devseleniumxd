package axacolpatria.descarga.DescargaSoat;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageObject extends Driver {

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(final WebDriver driver) {
		this.driver = driver;

	}

	// WebElements //

	public WebElement SeccionDescargar() {
		return getDriver().findElement((By.id("layout_9")));

	}

	public WebElement Placa() {
		return getDriver()
				.findElement((By.xpath("//*[@id=\"_soatdownload_WAR_soataxaportlet_numberDocumentAutomobile\"]")));

	}

	public WebElement NoDocumento() {

		return getDriver().findElement(By.xpath("//*[@id=\"_soatdownload_WAR_soataxaportlet_numberDocument\"]"));

	}

	public WebElement BotonDescargar() {

		return getDriver().findElement(By.className("btn-primary"));

	}
	public WebElement modal() {

		return getDriver().findElement(By.className("modal-content"));

	}
	public WebElement cerrarModal() {

		return getDriver().findElement(By.className("icon-axa_119"));

	}
	

	public WebElement Banner() {

		return getDriver().findElement(By.className("banner-info"));

	}
	public WebElement table() {

		return getDriver().findElement(By.tagName("table"));

	}

	public boolean IconoDescarga() throws InterruptedException {

		System.out.println("->> "+new Date());

		System.out.println("->> "+new Date());
	//	System.out.println("Tabla");
		Thread.sleep(5000);

		try {
			/*En caso que encuentre el modal
			 * Si no lo encuentra entrar�a al catch
			 */
			WebDriverWait wait = new WebDriverWait(driver,10);
					wait.until(ExpectedConditions.visibilityOf(this.modal()));
			System.out.println("modal!");
			cerrarModal().click();			
			return false;
		}catch(Exception e) {
			try {
				WebDriverWait wait = new WebDriverWait(driver,10);
				wait.until(ExpectedConditions.visibilityOf(this.table()));
				//En caso que encuentre la tabla
				System.out.println("tabla!");
				List<WebElement> lista = getDriver().findElement(By.tagName("table")).findElement(By.tagName("tbody"))
						.findElements(By.tagName("tr"));
				for (WebElement element : lista) {
					WebElement iconoDescarga = element.findElement(By.className("download-policy"));
					if (iconoDescarga != null) {
						iconoDescarga.click();
						Thread.sleep(10000);
					} else {

						System.out.println("El elemento no tiene url de descarga");
					}
				}
				return false;
			}catch(Exception ex){
				System.out.println("Descarga normal!");
				BotonDescargarClick();			
				return false;
			}
			
		}
		

	}
	// Services

	public void SeccionDescargarClick() {
		final WebDriverWait wait = new WebDriverWait(driver, 7000);
		wait.until(ExpectedConditions.visibilityOf(SeccionDescargar()));
		SeccionDescargar().click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		takeScreenShotTest(driver, "SeccionDescargar");

	}

	public void PlacaInsertar(final String Placa) throws InterruptedException {
		Placa().sendKeys(Placa);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(1400);
		takeScreenShotTest(driver, "InsertarPlaca");
	}

	public void NoDocumentoInsertar(final String NoDocumento) throws InterruptedException {

		NoDocumento().sendKeys(NoDocumento);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(1400);
		takeScreenShotTest(driver, "NoDocumentoInsertar");
	}

	public void BotonDescargarClick() throws InterruptedException {

		BotonDescargar().click();
		takeScreenShotTest(driver, "BotonDescargarClick");
		Thread.sleep(1000);
	}
}
