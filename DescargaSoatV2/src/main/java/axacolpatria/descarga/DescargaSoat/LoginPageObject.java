package axacolpatria.descarga.DescargaSoat;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageObject extends Driver {

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(final WebDriver driver) {
		this.driver = driver;

	}

	// WebElements //

	public WebElement SeccionDescargar() {
		return getDriver().findElement((By.id("layout_9")));

	}

	public WebElement Placa() {
		return getDriver()
				.findElement((By.xpath("//*[@id=\"_soatdownload_WAR_soataxaportlet_numberDocumentAutomobile\"]")));

	}

	public WebElement NoDocumento() {

		return getDriver().findElement(By.xpath("//*[@id=\"_soatdownload_WAR_soataxaportlet_numberDocument\"]"));

	}

	public WebElement BotonDescargar() {

		return getDriver().findElement(By.className("btn-primary"));

	}

	public WebElement Banner() {

		return getDriver().findElement(By.className("banner-info"));

	}
	

	public boolean IconoDescarga() throws InterruptedException {

		System.out.println("->> "+new Date());
		Thread.sleep(1000);
		System.out.println("->> "+new Date());
//		final String PolizasVigentes = "http://clientes.qa.axacolpatria.co:8081/web/my-axa/soat/descargar?p_p_id=soatdownload_WAR_soataxaportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_soatdownload_WAR_soataxaportlet_javax.portlet.action=getAvailableDownloads";
//		final String actualUrl = driver.getCurrentUrl();
//		final String URL = "http://clientes.qa.axacolpatria.co:8081/web/my-axa/soat/descargar";
		System.out.println("Tabla");
		Boolean isPresentTable = driver.findElements(By.tagName("table")).size() > 0 ;
		//WebElement tabla = getDriver().findElement(By.tagName("table"));
		System.out.println("Entrando al if");
		if (isPresentTable) {
			List<WebElement> lista = getDriver().findElement(By.tagName("table")).findElement(By.tagName("tbody"))
					.findElements(By.tagName("tr"));
			for (WebElement element : lista) {

				WebElement iconoDescarga = element.findElement(By.className("download-policy"));

//				WebElement variable = iconoDescarga.findElement(By.tagName("a"));

				if (iconoDescarga != null) {
					iconoDescarga.click();
					Thread.sleep(10000);
				} else {

					System.out.println("El elemento no tiene url de descarga");
				}

			}
		}else {
			System.out.println("Error");
			return false;
		}
		return false;

	}
	// Services

	public void SeccionDescargarClick() {
		final WebDriverWait wait = new WebDriverWait(driver, 7000);
		wait.until(ExpectedConditions.visibilityOf(SeccionDescargar()));
		SeccionDescargar().click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		takeScreenShotTest(driver, "SeccionDescargar");

	}

	public void PlacaInsertar(final String Placa) throws InterruptedException {
		Placa().sendKeys(Placa);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(1400);
		takeScreenShotTest(driver, "InsertarPlaca");
	}

	public void NoDocumentoInsertar(final String NoDocumento) throws InterruptedException {

		NoDocumento().sendKeys(NoDocumento);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(1400);
		takeScreenShotTest(driver, "NoDocumentoInsertar");
	}

	public void BotonDescargarClick() throws InterruptedException {

		BotonDescargar().click();
		takeScreenShotTest(driver, "BotonDescargarClick");
		Thread.sleep(1000);
	}
}
