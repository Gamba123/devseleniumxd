﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;


namespace Transacciones
{
    class Program
    {
        // Inicia el navegador
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "http://gestion.qa.axacolpatria.co:8080/web/admin/home";
           
            Csv(driver);
            Foto(driver);
        }

        //Lee el archivo CSV
        static void Csv(IWebDriver driver)
        {
            string[] lineas = File.ReadAllLines("C:\\Users\\daacevedoc\\source\\repos\\Transacciones\\Pruebaportal.csv");

            foreach(string linea in lineas){
                string[] valores = linea.Split('|');
                Login(driver, valores);
                Opcion(driver, valores);
                Filtro(driver, valores);
                Ejecutar(driver, valores);
            }
        }

        // Realiza captura de pantalla
        static void Foto(IWebDriver driver)
        {
            string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            ss.SaveAsFile("C:\\Users\\daacevedoc\\Desktop\\pantallazos\\selenium\\"+fecha+".png", ScreenshotImageFormat.Png);
        }

        static void Login(IWebDriver driver, string[] valores)
        {
            IWebElement usu = driver.FindElement(By.Id("_Authentication_username"));
            usu.SendKeys(valores[0]);
            Foto(driver);

            IWebElement pass = driver.FindElement(By.Id("_Authentication_password"));
            pass.SendKeys(valores[1]);
            Foto(driver);

            IWebElement confirm = driver.FindElement(By.ClassName("bt  n-default"));
            confirm.Click();
        }

        static void Opcion(IWebDriver driver, string[] valores)
        {
            System.Threading.Thread.Sleep(5000);

            IWebElement tran = driver.FindElement(By.Id(valores[2]));
            Foto(driver);
            tran.Click();

        }

        static void Filtro(IWebDriver driver, string[] valores)
        {
            System.Threading.Thread.Sleep(5000);

            IWebElement startDate = driver.FindElement(By.Id("_SoatPendingTransactionsPortlet_startDate"));
            startDate.SendKeys(valores[3]);
            Foto(driver);

            IWebElement endDate = driver.FindElement(By.Id("_SoatPendingTransactionsPortlet_endDate"));
            endDate.SendKeys(valores[4]);
            Foto(driver);

            IWebElement thirdParty = driver.FindElement(By.Id("_SoatPendingTransactionsPortlet_thirdParty"));
            thirdParty.Click();
            Foto(driver);

            IWebElement option = driver.FindElement(By.XPath("//*[@id='_SoatPendingTransactionsPortlet_thirdParty']/option[" + valores[5]+"]"));
            option.Click();
            Foto(driver);

            IWebElement bustra = driver.FindElement(By.ClassName("btn-primary"));
            bustra.Click();
            Foto(driver);
            System.Threading.Thread.Sleep(25000);
        }

        static void Ejecutar(IWebDriver driver, string[] valores)
        {
            IWebElement check = driver.FindElement(By.XPath("//*[@id='_SoatPendingTransactionsPortlet_formNumber0']"));
            check.Click();
            Foto(driver);

            IWebElement ejecute = driver.FindElement(By.Id("_SoatPendingTransactionsPortlet_btn-ejecute-tr"));
            ejecute.Click();
            Foto(driver);

            System.Threading.Thread.Sleep(5000);

            IWebElement rta = driver.FindElement(By.XPath("//*[@id='_SoatPendingTransactionsPortlet_state0']/i"));
            string rtaa = rta.GetAttribute("class");
            Foto(driver);

            while (rtaa == "icon-axa_353 error")
            {
                ejecute.Click();
                System.Threading.Thread.Sleep(5000);

                IWebElement rtaq = driver.FindElement(By.XPath("//*[@id='_SoatPendingTransactionsPortlet_state0']/i"));
                rtaa = rtaq.GetAttribute("class");
                System.Threading.Thread.Sleep(5000);
                Foto(driver);
            }
        }
    }
}
